package com.example;

import io.micronaut.core.util.CollectionUtils
import io.micronaut.data.r2dbc.operations.R2dbcOperations
import io.micronaut.http.annotation.Get
import io.micronaut.http.client.annotation.Client
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import io.micronaut.test.support.TestPropertyProvider
import io.reactivex.Flowable
//import org.testcontainers.containers.MySQLContainer
//import org.testcontainers.utility.DockerImageName
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import spock.lang.Shared
import spock.lang.Specification

import javax.inject.Inject

@MicronautTest
class BookControllerTest extends Specification {

    @Inject BookClient bookClient

    @Shared @Inject R2dbcOperations operations
    @Shared @Inject BookRepository bookRepository

    def setupSpec() {
        // tag::programmatic-tx[]
        Mono.from(operations.withTransaction(status ->
                Flux.from(bookRepository.saveAll([
                        new Book("The Stand", 1000),
                        new Book("The Shining", 400),
                        new Book("Along Came a Spider", 300),
                        new Book("Jurassic Park", 300),
                        new Book("Disclosure", 400)]))
                        .then()
        )).block()
        // end::programmatic-tx[]
    }

    void "test list books"() {
        when:
        List<Book> list = bookClient.list()

        then:
        list.size() == 5
    }

    @Client("/books")
    static interface BookClient {
        @Get("/")
        List<Book> list()
    }
}